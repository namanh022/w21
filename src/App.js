import logo from './logo.svg';
import './App.css';
import ComponentName from './componentName';

function App() {
  return (
    <div className="App">
      <ComponentName/>
      <ComponentName/>
    </div>
  );
}

export default App;
